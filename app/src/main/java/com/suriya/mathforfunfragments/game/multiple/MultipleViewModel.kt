package com.suriya.mathforfunfragments.game.multiple

import android.app.Application
import android.os.Handler
import android.util.Log
import androidx.lifecycle.*
import com.suriya.mathforfunfragments.database.Player
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao
import kotlinx.coroutines.launch
import kotlin.random.Random

class MultipleViewModel(
    argsPlayerName: String,
    val database: PlayerDatabaseDao,
    application: Application
) :
    AndroidViewModel(application) {
    //    Declare
    private var _num1 = MutableLiveData<Int>();
    val num1: LiveData<Int>
        get() = _num1

    private var _num2 = MutableLiveData<Int>();
    val num2: LiveData<Int>
        get() = _num2

    private var _sum = MutableLiveData<Int>();
    val sum: LiveData<Int>
        get() = _sum

    private var _result = MutableLiveData<String>();
    val result: LiveData<String>
        get() = _result

    private var _correct = MutableLiveData<Int>();
    val correct: LiveData<Int>
        get() = _correct

    private var _incorrect = MutableLiveData<Int>();
    val incorrect: LiveData<Int>
        get() = _incorrect

    private var _btnValue1 = MutableLiveData<Int>();
    val btnValue1: LiveData<Int>
        get() = _btnValue1

    private var _btnValue2 = MutableLiveData<Int>();
    val btnValue2: LiveData<Int>
        get() = _btnValue2

    private var _btnValue3 = MutableLiveData<Int>();
    val btnValue3: LiveData<Int>
        get() = _btnValue3

    private var _getPlayerName = MutableLiveData<String>();
    val getPlayerName: LiveData<String>
        get() = _getPlayerName

    private var _player = MutableLiveData<Player?>()
    private val player: LiveData<Player?>
        get() = _player

    private var i = 0

    init {
        _num1.value = 0
        _num2.value = 0
        _sum.value = 0
        _result.value = ""
        _correct.value = 0
        _incorrect.value = 0
        _btnValue1.value = 0
        _btnValue2.value = 0
        _btnValue3.value = 0
        _getPlayerName.value = argsPlayerName
        viewModelScope.launch {
            _player.value = getPlayerData(_getPlayerName.value.toString())
        }
        randomQuestion()
        initialButtonAnswer()
    }


    fun updatePlayerData() {
        viewModelScope.launch {
            updatePlayerData(player.value!!)
        }
    }

    // Database Handler
    private suspend fun getPlayerData(playerName: String): Player? {
        return database.get(playerName)
    }

    private suspend fun updatePlayerData(player: Player) {
        database.update(player)
    }

    fun randomQuestion() {
        _num1.value = Random.nextInt(1, 5)
        _num2.value = Random.nextInt(1, 5)
        _sum.value = _num1.value!! * _num2.value!!
    }

    fun answerName(answer: String) {
        _result.value = answer
    }

    fun isCorrect() {
        _correct.value = (correct.value)?.plus(1)
        viewModelScope.launch {
            player.value?.onCorrect()
        }
    }

    fun isIncorrect() {
        _incorrect.value = (incorrect.value)?.plus(1)
        viewModelScope.launch {
            player.value?.onIncorrect()
        }
    }

    fun checkAnswer(value: Int) {
        if (value == _sum.value) {
            answerName("ถูกต้อง")
            isCorrect()
        } else {
            answerName("ไม่ถูกต้อง")
            isIncorrect()
        }
        Handler().postDelayed({
            randomQuestion()
            initialButtonAnswer()
            Log.e(
                "PlayerData",
                "${player.value?.playerName} SCORE : ${player.value?.playerCorrect} - ${player.value?.playerIncorrect}")
        }, 250)
    }


    fun initialButtonAnswer() {
        i = Random.nextInt(1, 3)
        when (i) {
            1 -> {
                _btnValue1.value = _sum.value
                _btnValue2.value = _sum.value?.plus(1)
                _btnValue3.value = _sum.value?.plus(2)
            }
            2 -> {
                _btnValue2.value = _sum.value
                _btnValue1.value = _sum.value?.minus(1)
                _btnValue3.value = _sum.value?.plus(1)
            }
            3 -> {
                _btnValue3.value = _sum.value
                _btnValue1.value = _sum.value?.minus(1)
                _btnValue2.value = _sum.value?.minus(2)
            }
        }
    }
}
