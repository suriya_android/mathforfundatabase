package com.suriya.mathforfunfragments.game.multiple

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao

class MultipleViewModelFactory(
    private val argsPlayerName: String,
    private val dataSource: PlayerDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MultipleViewModel::class.java)) {
            return MultipleViewModel(argsPlayerName, dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}