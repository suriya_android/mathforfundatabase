package com.suriya.mathforfunfragments.game.minus

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao

class MinusViewModelFactory(
    private val argsPlayerName: String,
    private val dataSource: PlayerDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MinusViewModel::class.java)) {
            return MinusViewModel(argsPlayerName, dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}