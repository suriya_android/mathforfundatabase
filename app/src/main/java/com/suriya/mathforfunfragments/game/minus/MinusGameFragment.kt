package com.suriya.mathforfunfragments.game.minus

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.database.PlayerDatabase
import com.suriya.mathforfunfragments.databinding.FragmentMinusGameBinding
import com.suriya.mathforfunfragments.game.multiple.MultipleGameFragmentArgs
import com.suriya.mathforfunfragments.game.multiple.MultipleViewModelFactory
import com.suriya.mathforfunfragments.game.plus.PlusViewModel
import kotlin.random.Random

class MinusGameFragment : Fragment() {

    private lateinit var binding: FragmentMinusGameBinding
    private lateinit var viewModel: MinusViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment0
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_minus_game, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val args = MinusGameFragmentArgs.fromBundle(requireArguments())
        val viewModelFactory = MinusViewModelFactory(args.playerName, dataSource, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MinusViewModel::class.java)
        onBackPressed()
        binding.minusViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    private fun onBackPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val action = MinusGameFragmentDirections.actionMinusGameFragmentToTitleFragment2(
                viewModel.getPlayerName.value!!
            )
            viewModel.updatePlayerData()
            view?.findNavController()?.navigate(action)
        }
    }


}