package com.suriya.mathforfunfragments.game.plus


import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.database.PlayerDatabase
import com.suriya.mathforfunfragments.databinding.FragmentPlusGameBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class PlusGameFragment : Fragment() {

    private lateinit var binding: FragmentPlusGameBinding
    private lateinit var viewModel: PlusViewModel

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_plus_game,
            container,
            false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val args = PlusGameFragmentArgs.fromBundle(requireArguments())
        val viewModelFactory = PlusViewModelFactory(args.playerName, dataSource, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PlusViewModel::class.java)
        onBackPressed()
        binding.gameViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
        //checked
    }

    private fun onBackPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val action = PlusGameFragmentDirections.actionPlusGameFragmentToTitleFragment2(
                viewModel.getPlayerName.value!!
            )
            viewModel.updatePlayerData()
            view?.findNavController()?.navigate(action)
        }
    }

}