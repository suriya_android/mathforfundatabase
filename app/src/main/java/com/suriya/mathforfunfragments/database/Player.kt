package com.suriya.mathforfunfragments.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "player_table")
data class Player(
    @PrimaryKey(autoGenerate = true)
    var playerId: Long = 0L,

    @ColumnInfo(name = "player_name")
    var playerName: String = "",

    @ColumnInfo(name = "player_correct")
    var playerCorrect: Int = 0,

    @ColumnInfo(name = "player_incorrect")
    var playerIncorrect: Int = 0,

    @ColumnInfo(name = "player_summationScore")
    var playerSummationScore: Int = 0

) {
    fun onCorrect() {
        playerCorrect++
        playerSummationScore += 2
    }

    fun onIncorrect() {
        playerIncorrect++
        if (playerSummationScore <= 0) {
            playerSummationScore = 0
        } else {
            playerSummationScore--
        }
    }
}