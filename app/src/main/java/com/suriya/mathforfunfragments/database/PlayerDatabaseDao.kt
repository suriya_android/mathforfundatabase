package com.suriya.mathforfunfragments.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface PlayerDatabaseDao {
    @Insert
     suspend fun insert(player:Player)

    @Update
    suspend fun update(player:Player)

    @Query("SELECT * FROM player_table WHERE player_name = :playerName")
    suspend fun get(playerName: String):Player?

    @Query("DELETE FROM player_table")
    suspend fun clear()
    
    
    @Query("SELECT * FROM player_table ORDER BY (player_correct - player_incorrect) DESC")
    fun getAllPlayer(): LiveData<List<Player>>

    @Query("SELECT * FROM player_table WHERE player_name = :playerName")
    suspend fun getPlayerName(playerName : String) : Player?
}
