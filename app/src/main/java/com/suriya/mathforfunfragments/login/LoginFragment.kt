package com.suriya.mathforfunfragments.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.suriya.mathforfunfragments.login.LoginFragmentsDirections
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.database.PlayerDatabase
import com.suriya.mathforfunfragments.databinding.FragmentLoginBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragments.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragments : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel: LoginViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val viewModelFactory = LoginViewModelFactory(dataSource, application)
        val loginViewModel = ViewModelProvider(this, viewModelFactory).get(
            LoginViewModel::class.java
        )
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        viewModel.onLogin.observe(viewLifecycleOwner, Observer { onLogged ->
            if (onLogged) {
                val action =
                    LoginFragmentsDirections.actionLoginFragmentsToTitleFragment2(viewModel.playerName.value.toString())
                view?.findNavController()?.navigate(action)
                viewModel.loginCompleted()
            }
        })
        viewModel.playerNameNull.observe(viewLifecycleOwner, Observer { isNull ->
            if (isNull) {
                Toast.makeText(
                    application.applicationContext,
                    "ชื่อผู้ใช้งานห้ามว่าง",
                    Toast.LENGTH_SHORT
                ).show()
                viewModel.checkPlayerNameNullCompleted()
            }
        })

        binding.lifecycleOwner = viewLifecycleOwner
        binding.loginViewModel = loginViewModel
        return binding.root
        //checked
    }
}