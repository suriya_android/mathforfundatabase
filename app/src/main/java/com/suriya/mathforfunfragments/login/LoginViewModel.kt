package com.suriya.mathforfunfragments.login

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
import com.suriya.mathforfunfragments.database.Player
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao
import kotlinx.coroutines.launch

class LoginViewModel(val database: PlayerDatabaseDao, application: Application) :
    AndroidViewModel(application) {
    var playerNameNull = MutableLiveData<Boolean>()
    var onLogin = MutableLiveData<Boolean>()
    var playerName = MutableLiveData<String>()

    init {
        onLogin.value = false
        playerName.value = ""
    }

    fun checkPlayerFromDatabase(playerName: String) {
        viewModelScope.launch {
            if (!checkPlayerNameIsNull(playerName)) {
                if (database.getPlayerName(playerName) == null) {
                    Log.e("checkPlayer is nul func", "${checkPlayerNameIsNull(playerName)}")
                    val newPlayer = Player()
                    newPlayer.playerName = playerName
                    insert(newPlayer)
                    Log.e("PlayerName", "Player Name $playerName Not Found")
                }
                onLogin.value = true
            } else {
                playerNameNull.value = true
            }
        }
    }

    private fun checkPlayerNameIsNull(playerName: String): Boolean {
        if (playerName != "") {
            return false
        }
        return true
    }

    fun loginCompleted() {
        onLogin.value = false
    }

    fun checkPlayerNameNullCompleted() {
        playerNameNull.value = false
    }

    private suspend fun insert(newPlayer: Player) {
        database.insert(newPlayer)
    }
}