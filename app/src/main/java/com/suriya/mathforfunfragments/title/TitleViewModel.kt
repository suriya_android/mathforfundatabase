package com.suriya.mathforfunfragments.title

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.suriya.mathforfunfragments.database.Player
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao
import kotlinx.coroutines.launch

class TitleViewModel(
    argsPlayerName: String,
    val database: PlayerDatabaseDao,
    application: Application
) : ViewModel() {

    private val _playerName = MutableLiveData<String>()
    val playerName: LiveData<String>
        get() = _playerName

    private val _player = MutableLiveData<Player>()
    val player: LiveData<Player>
        get() = _player

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    private val _eventStartGame = MutableLiveData<Int>()
    val eventStatGame: LiveData<Int>
        get() = _eventStartGame

    fun onClickGamePlus() {
        _eventStartGame.value = 1
    }

    fun onClickGameMin() {
        _eventStartGame.value = 2
    }

    fun onClickGameMul() {
        _eventStartGame.value = 3
    }

    fun onEventCompleted() {
        _eventStartGame.value = 0
    }


    init {
        viewModelScope.launch {
            _player.value = database.get(argsPlayerName)
            _playerName.value = _player.value?.playerName
            _correct.value = _player.value?.playerCorrect
            _incorrect.value = _player.value?.playerIncorrect
        }
    }
}