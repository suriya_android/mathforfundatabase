package com.suriya.mathforfunfragments.title

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.database.PlayerDatabase
import com.suriya.mathforfunfragments.databinding.FragmentTitleBinding


private lateinit var viewModel: TitleViewModel
private lateinit var viewModelFactory: TitleViewModelFactory
private lateinit var binding: FragmentTitleBinding

class TitleFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_title, container, false)

        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val args = TitleFragmentArgs.fromBundle(requireArguments())
        val viewModelFactory = TitleViewModelFactory(args.playerName, dataSource, application)


        // Do
        setHasOptionsMenu(true)

        viewModel = ViewModelProvider(this, viewModelFactory).get(TitleViewModel::class.java)
        viewModel.eventStatGame.observe(viewLifecycleOwner, Observer { startGame ->
            when (startGame) {
                1 -> {
                    val action = TitleFragmentDirections.actionTitleFragment2ToPlusGameFragment(
                        viewModel.playerName.value!!
                    )

                    view?.findNavController()?.navigate(action)
                    viewModel.onEventCompleted()
                }
                2 -> {
                    val action = TitleFragmentDirections.actionTitleFragment2ToMinusGameFragment(
                        viewModel.playerName.value!!
                    )
                    view?.findNavController()?.navigate(action)
                    viewModel.onEventCompleted()
                }
                3 -> {
                    val action =
                        TitleFragmentDirections.actionTitleFragment2ToMultipleGameFragment(
                            viewModel.playerName.value!!
                        )
                    view?.findNavController()?.navigate(action)
                    viewModel.onEventCompleted()
                }
            }
        })
        binding.titleViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
        //checked
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            requireView().findNavController()
        ) || super.onOptionsItemSelected(item)
    }
}