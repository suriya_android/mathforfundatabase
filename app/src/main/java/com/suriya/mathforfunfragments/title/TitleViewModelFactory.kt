package com.suriya.mathforfunfragments.title

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao
import java.lang.IllegalArgumentException

class TitleViewModelFactory(
    private val playerName: String,
    private val dataSource: PlayerDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TitleViewModel::class.java)) {
            return TitleViewModel(playerName, dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}