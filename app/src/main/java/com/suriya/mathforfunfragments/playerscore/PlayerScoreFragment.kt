package com.suriya.mathforfunfragments.playerscore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.database.PlayerDatabase
import com.suriya.mathforfunfragments.databinding.FragmentPlayerScoreBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlayerScoreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlayerScoreFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private lateinit var binding: FragmentPlayerScoreBinding
    private lateinit var viewModel: PlayerScoreViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_player_score, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        val viewModelFactory = PlayerScoreViewModelFactory(dataSource, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PlayerScoreViewModel::class.java)
        val adapter = PlayerScoreAdapter()
        viewModel.players.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })
        viewModel.onClearData.observe(viewLifecycleOwner, Observer { isCleared ->
            if (isCleared) {
                viewModel.clearData()
                viewModel.onClearDataCompleted()
            }
        })
        viewModel.onBackButton.observe(viewLifecycleOwner, Observer { isPressed ->
            if (isPressed) {
                view?.findNavController()?.navigate(
                    PlayerScoreFragmentDirections.actionPlayerScoreFragmentToLoginFragments("")
                )
                viewModel.onBackButtonCompleted()
            }
        })
        binding.playerLists.adapter = adapter
        binding.lifecycleOwner = this
        binding.playerScoreViewModel = viewModel
        return binding.root
        //checked
    }
}