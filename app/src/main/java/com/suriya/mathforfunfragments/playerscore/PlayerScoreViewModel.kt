package com.suriya.mathforfunfragments.playerscore

import android.app.Application
import android.provider.SyncStateContract.Helpers.insert
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.suriya.mathforfunfragments.database.Player
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao
import kotlinx.coroutines.launch

class PlayerScoreViewModel(val database: PlayerDatabaseDao, application: Application) :
    AndroidViewModel(application) {
    val players = database.getAllPlayer()
    var onClearData = MutableLiveData<Boolean>()
    var onBackButton = MutableLiveData<Boolean>()

    init {
    }


    fun clearData() {
        onClearData.value = true
        viewModelScope.launch {
            database.clear()
        }

    }

    fun onClearDataCompleted() {
        onClearData.value = false
    }

    fun onBackButton() {
        onBackButton.value = true
    }

    fun onBackButtonCompleted() {
        onBackButton.value = false
    }
}