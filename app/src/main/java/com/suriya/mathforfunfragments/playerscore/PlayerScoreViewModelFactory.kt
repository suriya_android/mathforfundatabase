package com.suriya.mathforfunfragments.playerscore

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.suriya.mathforfunfragments.database.PlayerDatabaseDao

class PlayerScoreViewModelFactory(private val dataSource: PlayerDatabaseDao, private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlayerScoreViewModel::class.java)) {
            return PlayerScoreViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}