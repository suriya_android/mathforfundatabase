package com.suriya.mathforfunfragments.playerscore

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.TextItemViewHolder
import com.suriya.mathforfunfragments.database.Player

class PlayerScoreAdapter : RecyclerView.Adapter<TextItemViewHolder>() {
    var data = listOf<Player>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: TextItemViewHolder, position: Int) {
        val item = data[position]
        holder.textView.text = "ผู้เล่น ${item.playerName}  :  ${item.playerSummationScore} คะแนน"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.text_item_view, parent, false) as TextView
        return TextItemViewHolder(view)
    }


}